package Entities;

public class Quiz {
    private int id;
    private String quiz_name;
    private Structure structure;
    private int score;

    public Quiz(int id, Structure structure, int score, String quiz_name) {
        this.id = id;
        this.structure = structure;
        this.score = score;
        this.quiz_name = quiz_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getQuiz_name() {
        return quiz_name;
    }

    public void setQuiz_name(String quiz_name) {
        this.quiz_name = quiz_name;
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "id=" + id +
                ", structure='" + structure + '\'' +
                ", score=" + score +
                ", quiz_name='" + quiz_name + '\'' +
                '}';
    }
}
