
package Entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import org.apache.commons.lang.builder.ToStringBuilder;

public class Page {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("elements")
    @Expose
    private List<Element> elements = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Page() {
    }

    /**
     *
     * @param name
     * @param elements
     */
    public Page(String name, List<Element> elements) {
        super();
        this.name = name;
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Page withName(String name) {
        this.name = name;
        return this;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    public Page withElements(List<Element> elements) {
        this.elements = elements;
        return this;
    }

    @Override
    public String toString() {
        return "Page{" +
                "name='" + name + '\'' +
                ", elements=" + elements +
                '}';
    }
}
