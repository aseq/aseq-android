
package Entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import org.apache.commons.lang.builder.ToStringBuilder;

public class Structure {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("pages")
    @Expose
    private List<Page> pages = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Structure() {
    }

    /**
     *
     * @param title
     * @param pages
     */
    public Structure(String title, List<Page> pages) {
        super();
        this.title = title;
        this.pages = pages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Structure withTitle(String title) {
        this.title = title;
        return this;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public Structure withPages(List<Page> pages) {
        this.pages = pages;
        return this;
    }

    @Override
    public String toString() {
        return "Structure{" +
                "title='" + title + '\'' +
                ", pages=" + pages +
                '}';
    }
}
