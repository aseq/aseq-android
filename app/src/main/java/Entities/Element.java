
package Entities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import org.apache.commons.lang.builder.ToStringBuilder;

public class Element {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("choices")
    @Expose
    private List<String> choices = null;
    @SerializedName("maxSize")
    @Expose
    private Integer maxSize;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Element() {
    }

    /**
     *
     * @param items
     * @param choices
     * @param name
     * @param type
     * @param maxSize
     */
    public Element(String type, String name, List<String> choices, Integer maxSize, List<Item> items) {
        super();
        this.type = type;
        this.name = name;
        this.choices = choices;
        this.maxSize = maxSize;
        this.items = items;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Element withType(String type) {
        this.type = type;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Element withName(String name) {
        this.name = name;
        return this;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public Element withChoices(List<String> choices) {
        this.choices = choices;
        return this;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public Element withMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Element withItems(List<Item> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return "Element{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", choices=" + choices +
                ", maxSize=" + maxSize +
                ", items=" + items +
                '}';
    }
}
