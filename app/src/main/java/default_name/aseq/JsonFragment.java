package default_name.aseq;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class JsonFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_json, container, false);
    }

    public void getStudents(View view) {
        StudentsTask task = new StudentsTask();
        task.execute("https://api.myjson.com/bins/x4bke");

    }

    public void getQuizHistories(View view) {
        QuizHistoriesTask task = new QuizHistoriesTask();
        String[] string = new String[2];
        string[0] = "https://api.myjson.com/bins/x4bke";
        string[1] = "Id-ul studentlui";
        task.execute(string);
    }

    public void getFeedbackComments(View view) {
        FeedbackCommentsTask task = new FeedbackCommentsTask();
        String[] string = new String[2];
        string[0] = "https://api.myjson.com/bins/x4bke";
        string[1] = "Id-ul quizHistoryului";
        task.execute(string);

    }
}
