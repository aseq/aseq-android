package default_name.aseq;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StudentsTask extends AsyncTask<String, Void, ArrayList<Map<String, String>>> {

    @Override
    protected ArrayList<Map<String, String>> doInBackground(String... params) {
        ArrayList<Map<String,String>> studentsList = null;
        try {
            URL url = new URL(params[0]);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            InputStream stream = http.getInputStream();
            InputStreamReader rea = new InputStreamReader(stream);
            BufferedReader reader = new BufferedReader(rea);
            String linie="";
            StringBuilder builder = null;
            while((linie=reader.readLine()) != null) {
                builder.append(linie);
            }

            studentsList = new ArrayList<Map<String, String>>();
            JSONArray students = new JSONArray(builder.toString());
            for(int i=0;i<students.length();i++) {
                JSONObject obj = (JSONObject)students.get(i);
                Map<String,String> student = new HashMap<String,String>();
                student.put("firstName",obj.getString("firstName"));
                student.put("lastName",obj.getString("lastName"));
                student.put("email",obj.getString("email"));
                student.put("phone",obj.getString("phone"));
                student.put("score",obj.getString("score"));
                student.put("group",obj.getString("group"));
                studentsList.add(student);

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return studentsList;
    }

    @Override
    protected void onPostExecute(ArrayList<Map<String, String>> result) {
        // aici vine codul care pune elementele in lista (result e lista de elemente)

    }
}
