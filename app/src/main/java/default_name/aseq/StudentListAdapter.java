package default_name.aseq;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class StudentListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<User> users;

    public StudentListAdapter(Context context, List<User> users) {
        super(context, -1);
        this.context = context;
        this.users = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custom_layout_student, parent, false);
        TextView textViewNume = (TextView) rowView.findViewById(R.id.nume);
        TextView textViewEmail = (TextView) rowView.findViewById(R.id.email);
        TextView textViewTel = (TextView) rowView.findViewById(R.id.telefon);
        TextView textViewGrupa = (TextView) rowView.findViewById(R.id.grupa);
        TextView textViewScor = (TextView) rowView.findViewById(R.id.scor);

        textViewNume.setText(users.get(position).getUsername());
        textViewEmail.setText(users.get(position).getEmail());
        textViewTel.setText(users.get(position).getPhone());
//        textViewGrupa.setText(users.get(position).getGrupa());
//        textViewScor.setText(users.get(position).getScor());

        return rowView;
    }
}
