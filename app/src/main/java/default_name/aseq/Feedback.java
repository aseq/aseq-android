package default_name.aseq;

public class Feedback {
    private boolean type;
    private String text;

    public Feedback(boolean type, String text) {
        this.type = type;
        this.text = text;
    }

    public boolean isType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
