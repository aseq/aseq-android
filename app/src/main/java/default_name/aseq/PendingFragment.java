package default_name.aseq;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.TextView;

import Entities.Quiz;
import java.util.List;


import Routing.IBackendInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PendingFragment extends Fragment {

    private TextView profileResult;
    private String getResult;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pending, container, false);
        profileResult = (TextView) v.findViewById(R.id.quizText);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://frizeriamica.ro:3000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IBackendInterface backend = retrofit.create(IBackendInterface.class);

//        Call<List<Quiz>> call = backend.getQuizes();
//
//        call.enqueue(new Callback<List<Quiz>>() {
//            @Override
//            public void onResponse(Call<List<Quiz>> call, Response<List<Quiz>> response) {
//                if(!response.isSuccessful()) {
////                    profileResult.setText("Code: " + response.code());
//                    Log.e("BANANA", "\n" + response.code() + "\n");
//                    return;
//                }
//
//                List<Quiz> quizes = response.body();
//
//                profileResult.setText("TextSet");
//
//                for(Quiz quiz : quizes) {
//                    String content = "";
//                    content += "QuizID: " + quiz.getId()+ "\n";
//                    content += "QuizName : " + quiz.getQuiz_name() + "\n";
//                    content += "Structure : " + quiz.getStructure().toString() + "\n";
//                    content += "Score : " + quiz.getScore() + "\n\n";
//
//                    getResult += content;
//                }
//
//
//
//                profileResult.setText(getResult);
//                Log.w("BANANA", getResult);
//            }
//
//            @Override
//            public void onFailure(Call<List<Quiz>> call, Throwable t) {
//                profileResult.setText(t.getMessage());
//                Log.e("BANANA", "\n" + t.getMessage());
//            }
//        });

        Call call = backend.getQuiz();

        call.enqueue(new Callback<Quiz>() {
            @Override
            public void onResponse(Call<Quiz> call, Response<Quiz> response) {
                Quiz quiz = response.body();

                profileResult.setText(quiz.toString());

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                profileResult.setText(t.getMessage());
                Log.e("BANANA", "\n" + t.getMessage());
            }
        });


        return v;
    }
}
