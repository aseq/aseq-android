package default_name.aseq;

public class History {
    private int id;
    private int score;
    private String responseTime;
    private int rating;
    private int quiz;

    public History(int id, int score, String responseTime, int rating, int quiz) {
        this.id = id;
        this.score = score;
        this.responseTime = responseTime;
        this.rating = rating;
        this.quiz = quiz;
    }

    public int getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public int getRating() {
        return rating;
    }

    public int getQuiz() {
        return quiz;
    }
}
