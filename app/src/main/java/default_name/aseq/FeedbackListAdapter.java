package default_name.aseq;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class FeedbackListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<Feedback> feedbacks;

    public FeedbackListAdapter(Context context, List<Feedback> feedbacks) {
        super(context, -1);
        this.context = context;
        this.feedbacks = feedbacks;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custom_layout_history, parent, false);
        TextView textViewPositive = (TextView) rowView.findViewById(R.id.positive);
        TextView textViewNegative = (TextView) rowView.findViewById(R.id.negative);

        if(feedbacks.get(position).isType())
            textViewPositive.setText(feedbacks.get(position).getText());
        if(!feedbacks.get(position).isType())
            textViewNegative.setText(feedbacks.get(position).getText());

        return rowView;
    }
}
