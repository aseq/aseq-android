package default_name.aseq;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileFragment extends Fragment {

    private TextView profileResult;
    private String getResult = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        profileResult = getView().findViewById(R.id.profile_result);
//        profileResult = (TextView) getActivity().findViewById(R.id.profile_result);

        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        profileResult = (TextView) v.findViewById(R.id.profile_result);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<User>> call = jsonPlaceHolderApi.getUsers();

        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(!response.isSuccessful()) {
                    profileResult.setText("Code: " + response.code());
                    return;
                }

                List<User> users = response.body();

                profileResult.setText("TextSet");

                for(User user : users) {
                    String content = "";
                    content += "UserId: " + user.getUserId() + "\n";
                    content += "Username: " + user.getUsername() + "\n";
                    content += "Email: " + user.getEmail() + "\n";
                    content += "Phone: " + user.getPhone() + "\n\n";

                    getResult += content;
                }
                
                profileResult.setText(getResult);
//                Log.w("MINE", getResult);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                profileResult.setText(t.getMessage());
            }
        });




        return v;
    }
}
