package default_name.aseq;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class HistoryListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<History> histories;

    public HistoryListAdapter(Context context, List<History> histories) {
        super(context, -1);
        this.context = context;
        this.histories = histories;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custom_layout_history, parent, false);
        TextView textViewQuiz = (TextView) rowView.findViewById(R.id.quiz);
        TextView textViewRating = (TextView) rowView.findViewById(R.id.rating);
        TextView textViewScore = (TextView) rowView.findViewById(R.id.scor);
        TextView textViewTime = (TextView) rowView.findViewById(R.id.time);

        textViewQuiz.setText(histories.get(position).getQuiz());
        textViewRating.setText(histories.get(position).getRating());
        textViewScore.setText(histories.get(position).getScore());
        textViewTime.setText(histories.get(position).getResponseTime());

        return rowView;
    }
}
