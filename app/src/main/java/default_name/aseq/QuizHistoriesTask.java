package default_name.aseq;

import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QuizHistoriesTask extends AsyncTask<String[], Void, ArrayList<Map<String, String>>> {

    @Override
    protected ArrayList<Map<String, String>> doInBackground(String[]... params) {
        ArrayList<Map<String,String>> qhList = null;
        try {
            URL url = new URL(params[0][0]);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            InputStream stream = http.getInputStream();
            InputStreamReader rea = new InputStreamReader(stream);
            BufferedReader reader = new BufferedReader(rea);
            String linie="";
            StringBuilder builder = null;
            while((linie=reader.readLine()) != null) {
                builder.append(linie);
            }

            qhList = new ArrayList<Map<String, String>>();
            JSONArray students = new JSONArray(builder.toString());
            for(int i=0;i<students.length();i++) {
                JSONObject obj = (JSONObject)students.get(i);
                if (obj.getString("id") == params[0][1]) {
                    JSONArray qh = obj.getJSONArray("quizHistories");
                    for(int j=0;j<qh.length();j++) {
                        JSONObject obj2 = (JSONObject)qh.get(i);
                        Map<String,String> quizHistory = new HashMap<String,String>();
                        quizHistory.put("score",obj2.getString("score"));
                        quizHistory.put("responseTime",obj2.getString("responseTime"));
                        quizHistory.put("rating",obj2.getString("rating"));
                        qhList.add(quizHistory);
                    }
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return qhList;
    }

    @Override
    protected void onPostExecute(ArrayList<Map<String, String>> result) {
        // aici vine codul care pune elementele in lista (result e lista de elemente)

    }
}
