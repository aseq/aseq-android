package default_name.aseq;

import com.google.gson.annotations.SerializedName;

public class User {
    private int userId;
    private String username;
    private String email;
    private String phone;

//    @SerializedName("body")
//    private String text;


    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
