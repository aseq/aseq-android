package Routing;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

import Entities.Quiz;

public interface IBackendInterface {

    @GET("quizes")
    Call<List<Quiz>> getQuizes();

    @GET("quiz/6")
    Call<Quiz> getQuiz();

}
